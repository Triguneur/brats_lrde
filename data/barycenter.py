# Compute barycenter of a tumor in brains MRI (BraTS2020)


import numpy as np
from random import randint
import matplotlib.pyplot as plt


epsilon = 1e-8


def fast_display(*img2dlst):
    plt.figure(figsize=(16,8))
    nbimg = len(img2dlst)
    cols = min(9, nbimg)
    rows = (nbimg // cols) + 1
    for ii, img2d in enumerate(img2dlst):
        plt.subplot(rows, cols, 1+ii)
        plt.imshow(img2d)
    plt.show()

def compute_barycenter1d(data: np.ndarray) -> int:
    total = 0
    current = 0
    for i in range(len(data)):
        total += data[i] + epsilon
        current += data[i] * i
    return int(current // total)


def compute_barycenter2d(data: np.ndarray) -> [int, int]:
    A = np.sum(data, axis=0)
    B = np.sum(data, axis=1)
    return [compute_barycenter1d(A), compute_barycenter1d(B)]


def compute_barycenter3d(data: np.ndarray) -> [int, int, int]:
    A = np.sum(data, axis=0)
    B = np.sum(data, axis=1)
    return [compute_barycenter2d(B)[1], compute_barycenter2d(A)[1], compute_barycenter2d(A)[0]]


def make_cropx64(data: np.ndarray, barycenter: [int, int, int], size=64) -> np.ndarray:

    b_coord = np.subtract(barycenter, (32,32,32))
    b_coord = b_coord.clip(min=0)
    if b_coord[0] > 255-size or b_coord[1] > 255-size or b_coord[2] > 155-size:
        raise ValueError("Need to fix b_coord")

    result = data[b_coord[0]:b_coord[0]+size, b_coord[1]:b_coord[1]+size, b_coord[2]:b_coord[2]+size]
    return result


def in_range(data: np.ndarray, i: int, shape: int, range=None) -> int:
    if range is None:
        range = [0, data.shape[shape]]
    if i < range[0]:
        return range[0]
    elif i > range[1]:
        return range[1]
    else:
        return i

def make_crop_rand(data: np.ndarray, size=(32,32,32), number=10, n_init=500, ratio=0.35) -> [([int, int, int], np.ndarray)]:

    result = []
    count = 0
    check = 0
    while count < number:
        x = in_range(data, randint(0, data.shape[0] - size[0]), 0)
        y = in_range(data, randint(0, data.shape[1] - size[1]), 1)
        z = in_range(data, randint(0, data.shape[2] - size[2]), 2)
        tmp = data[x:x+size[0], y:y+size[1], z:z+size[2]]
        if (np.mean(tmp) < ratio and check < n_init):
            check += 1
            continue
            
        check = 0
        result.append(([x,y,z],tmp))
        count += 1
    return result



def make_crop_coord(data: np.ndarray, coord: [int,int,int], size=(32,32,32)) -> np.ndarray:
    if len(coord) != 3:
        raise ValueError("coord must be 3d")

    x = in_range(data, coord[0], 0, range=[0, data.shape[0] - size[0]])
    y = in_range(data, coord[1], 1, range=[0, data.shape[1] - size[1]])
    z = in_range(data, coord[2], 2, range=[0, data.shape[2] - size[2]])
    tmp = data[x:x+size[0], y:y+size[1], z:z+size[2]]
    # result = data[coord[0]:coord[0]+size[0], coord[1]:coord[1]+size[1], coord[2]:coord[2]+size[2]]
    # return result
    return tmp

def display_3d(data, important_slice=None):
    '''
    Vous allez me dire que c'est etonnant, mais cette fonction est completement buggee.
    Faire tres attention aux shapes envoye a la fonction.
    Le probleme c'est que je sais pas pour quelles shapes la fonction se comporte correctement
    '''
    by_line = 5
    for i in range(0,len(data)-by_line+1,by_line):
        if (important_slice != None) and (i <= important_slice <= i + 4):
            print ("ESSENTIAL SLICE IS HERE : ", important_slice)
        print(f"slice: {i}, slice+{by_line-1}: {i+by_line-1}, max slice: {len(data)}")
        fast_display(data[:,:,i], data[:,:,i+1], data[:,:,i+2], data[:,:,i+3], data[:,:,i+4])
        
def get_important_slice(barycenter, slc, size=64):
    return slc - (barycenter[2] - size//2)


def fill_with_zeros(data, new_shape):
    result = np.zeros(new_shape)
    return result + data


def add_value(data: np.ndarray, new_shape=(256,256,192), fill_with=0) -> np.ndarray:
    if (data.ndim != 3):
        raise ValueError("data must be 3d array")
    if (data.shape > new_shape):
        raise ValueError("data_shape must be < new_shape")
    A = np.full((data.shape[0], data.shape[1], new_shape[2] - data.shape[2]), fill_with, dtype=data.dtype)
    result = np.dstack((data, A))
    A = np.full((new_shape[0] - data.shape[0], data.shape[1], new_shape[2]), fill_with, dtype=data.dtype)
    result = np.vstack((result, A))
    A = np.full((new_shape[0], new_shape[1] - data.shape[1], new_shape[2]), fill_with, dtype=data.dtype)
    result = np.hstack((result, A))
    return result
