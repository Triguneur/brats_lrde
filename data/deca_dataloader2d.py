import os
import json


import numpy as np
from torch.utils.data import Dataset
import torch
import nibabel as nib
from tqdm import tqdm

from utils import compute_normalization, crop_center


class DecathlonDataset2d(Dataset):


    def __init__(self,
        in_paths,
        size_slide,
        nb_slides,
        size_training,
        first_index = 0,
        x_path='./xtrain.hdf5',
        y_path='./ytrain.hdf5',
        force_compute=False,
        transforms=None):

        self.in_paths = in_paths
        self.size_slide = size_slide
        self.nb_slides = nb_slides
        self.size_training = size_training
        self.first_index = first_index
        self.x_path=x_path
        self.y_path=y_path
        self.x_data = None
        self.y_data = None
        self.is_compute = False
        self.transforms = transforms
        self.len_dataset = size_training * nb_slides * len(in_paths)

        self.initialization(force_compute=force_compute)
        self.compute_dataset(force_compute=force_compute)


    def __len__(self):
        return self.len_dataset


    def __getitem__(self, item):

        if torch.is_tensor(item):
            item = item.tolist()

        data = np.copy(self.x_data[item,:,:,:])
        label = np.copy(self.y_data[item,:,:,:])

        if self.transforms:
            data = self.transforms(data)
            label = self.transforms(label)

        result = {'data': data, 'label': label}

        return result


    def initialization(self, force_compute=False):

        if os.path.isfile(self.x_path) and os.path.isfile(self.y_path) and not force_compute:
            mode = 'r'
            self.is_compute = True
        else:
            mode = "w+"
            self.is_compute = False

        size_slide = self.size_slide
        self.x_data = np.memmap(self.x_path, dtype='float32', mode=mode,
                        shape=(self.len_dataset, size_slide[0], size_slide[1], 1))
        self.y_data = np.memmap(self.y_path, dtype='float32', mode=mode,
                        shape=(self.len_dataset, size_slide[0], size_slide[1], 1))


    def compute_folder(self, in_path, index):

        with open(os.path.join(in_path, 'dataset.json'), 'r') as f:
            data = json.load(f)

        for i in tqdm(range(self.first_index, self.first_index + self.size_training), total=(self.size_training - self.first_index)):
            
            path_label = os.path.join(in_path, data['training'][i]['label'])
            path_data = os.path.join(in_path, data['training'][i]['image'])

            current_label = np.clip(compute_normalization(nib.load(path_label).get_fdata()), 0, 1)
            current_image = compute_normalization(nib.load(path_data).get_fdata())
            
            start_index = (current_image.shape[2] - self.nb_slides) // 2

            __j = 0
            for j in range(start_index, self.nb_slides + start_index):

                if len(current_image.shape) == 3:
                    self.x_data[index + (i * self.nb_slides) + __j, :, :, 0] =\
                                                                crop_center(current_image[:, :, j], self.size_slide)

                elif len(current_image.shape) == 4:
                    self.x_data[index + (i * self.nb_slides) + __j, :, :, 0] =\
                                                                crop_center(current_image[:, :, j, 0], self.size_slide)

                self.y_data[index + (i * self.nb_slides) + __j, :, :, 0] =\
                                                            crop_center(current_label[: ,:, j], self.size_slide)

                __j += 1


    def compute_dataset(self, force_compute=False):

        if self.x_data is None or self.y_data is None:
            raise ValueError("Need to initialize first")

        if not force_compute and self.is_compute:
            print("Skip computation... If you want to force computation, you can use force_compute=True as args")
            return

        index = 0
        for in_path in self.in_paths:
            
            self.compute_folder(in_path, index)
            index += (self.size_training * self.nb_slides)

        np.memmap.flush(self.x_data)
        del self.x_data
        np.memmap.flush(self.y_data)
        del self.y_data
        size_slide = self.size_slide
        mode = 'r'
        self.x_data = np.memmap(self.x_path, dtype='float32', mode=mode,
                        shape=(self.len_dataset, size_slide[0], size_slide[1], 1))
        self.y_data = np.memmap(self.y_path, dtype='float32', mode=mode,
                        shape=(self.len_dataset, size_slide[0], size_slide[1], 1))

