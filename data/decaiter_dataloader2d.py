import os
import json

import numpy as np
from torch.utils.data import IterableDataset
import torch
import nibabel as nib

from utils import *


class IterDecathlonDataset(IterableDataset):




    def __init__(self, in_paths, nb_images, start_index=0, transform=None):


        # TODO : Make a check for index and datassize

        self.in_paths = in_paths
        self.nb_images = nb_images
        self.transform = transform
        self.start_index = start_index
        self.datas = list()

        for path in in_paths:
            with open(os.path.join(path, 'dataset.json')) as f:
                self.datas.append(json.load(f))


    def __iter__(self, testing=False):

        RATIO_AREA = 0.2
        for i in range(len(self.datas)):
            data = self.datas[i]
            for j in range(self.start_index, self.start_index + self.nb_images):
                current_image = nib.load(os.path.join(self.in_paths[i], data['training'][j]['image'])).get_fdata()
                current_label = nib.load(os.path.join(self.in_paths[i], data['training'][j]['label'])).get_fdata()

                if len(current_image.shape) != 3:
                    # TODO : Make a generic processing
                    current_image = current_image[:,:,:,0]

                for k in range(current_image.shape[2]):
                    '''
                    if np.sum(current_image[:,:,k] > 0) \
                                    >= RATIO_AREA * current_image.shape[0] * current_image.shape[1]:
                        continue
                    else:
                    '''
                    yield self.__post_processing(current_image[:,:,k], current_label[:,:,k])

                if testing:
                    yield None


    def __post_processing(self, image, label):
        if self.transform:
            image = self.transform(image)
            label = self.transform(label)

        result = {'data': image, 'label': label}
        return result

