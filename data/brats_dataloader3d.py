import os

from torch.utils.data import Dataset
import numpy as np
import torch
import nibabel as nib
from tqdm import tqdm

from data.barycenter import *


class BraTsDataset3d(Dataset):

    def __init__(
            self,
            in_path,
            nb_brains,
            start_index=0,
            x_path='./xtrain.hdf5',
            y_path='./ytrain.hdf5',
            nb_crop=1,
            size_crop=(240, 240, 155),
            n_init=1000,
            ratio=0.1,
            force_compute=False,
            verbose=False,
            transform=None):

        self.in_path = in_path
        self.nb_crop = nb_crop
        self.size_crop = size_crop
        self.nb_brains = nb_brains
        self.start_index = start_index
        self.n_init = n_init
        self.ratio = ratio
        self.size = self.nb_brains * self.nb_crop
        self.x_path = x_path
        self.y_path = y_path
        self.x_data = None
        self.y_data = None
        self.is_compute = False
        self.initialize(force=force_compute)
        self.compute_folder(force=force_compute, verbose=verbose)
        self.transform = transform

    def __len__(self):
        return self.size

    def __getitem__(self, item):

        if torch.is_tensor(item):
            item = item.tolist()

        data = np.copy(self.x_data[item, :, :, :, :])
        label = np.copy(self.y_data[item, :, :, :, :])

        sample = {'data': data, 'label': label}

        if self.transform:
            sample = self.transform(sample)

        return sample

    def initialize(self, force=False):
        if os.path.isfile(self.x_path) and os.path.isfile(self.y_path) and not force:
            mode = 'r'
            self.is_compute = True
        else:
            mode = "w+"
            self.is_compute = False

        size_crop = self.size_crop
        self.x_data = np.memmap(self.x_path, dtype='float32', mode=mode,
                                shape=(self.size, size_crop[0], size_crop[1], size_crop[2], 4))
        self.y_data = np.memmap(self.y_path, dtype='float32', mode=mode,
                                shape=(self.size, size_crop[0], size_crop[1], size_crop[2], 1))

    @staticmethod
    def compute_normalization(data):
        nonzeros = data[data > 0]
        mu = np.mean(nonzeros)
        sigma = np.std(nonzeros)
        if sigma == 0:
            result = data
        else:
            result = np.clip((data - mu) / (12.0 * sigma), -1.0, 1.0)
        return result


    def compute_one_brain_crops(self, root_path, item):

        seg = nib.load(root_path + "_seg.nii.gz").get_fdata()
        seg = np.clip(seg, 0, 1)
        seg_crop = make_crop_rand(seg, size=self.size_crop, number=self.nb_crop, n_init=self.n_init, ratio=self.ratio)

        flair = self.compute_normalization(nib.load(root_path + "_flair.nii.gz").get_fdata())
        t1 = self.compute_normalization(nib.load(root_path + "_t1.nii.gz").get_fdata())
        t1ce = self.compute_normalization(nib.load(root_path + "_t1ce.nii.gz").get_fdata())
        t2 = self.compute_normalization(nib.load(root_path + "_t2.nii.gz").get_fdata())

        for e in seg_crop:
            self.x_data[item, :, :, :, 0] = make_crop_coord(flair, coord=e[0], size=self.size_crop)
            self.x_data[item, :, :, :, 1] = make_crop_coord(t1, coord=e[0], size=self.size_crop)
            self.x_data[item, :, :, :, 2] = make_crop_coord(t1ce, coord=e[0], size=self.size_crop)
            self.x_data[item, :, :, :, 3] = make_crop_coord(t2, coord=e[0], size=self.size_crop)
            self.y_data[item, :, :, :, 0] = e[1]

            item += 1

        return item

    def compute_one_brain_nocrop(self, root_path, item):

        self.y_data[item, :, :, :, 0] = np.clip(nib.load(root_path + "_seg.nii.gz").get_fdata(), 0, 1)
        self.x_data[item, :, :, :, 0] = self.compute_normalization(nib.load(root_path + "_flair.nii.gz").get_fdata())
        self.x_data[item, :, :, :, 1] = self.compute_normalization(nib.load(root_path + "_t1.nii.gz").get_fdata())
        self.x_data[item, :, :, :, 2] = self.compute_normalization(nib.load(root_path + "_t1ce.nii.gz").get_fdata())
        self.x_data[item, :, :, :, 3] = self.compute_normalization(nib.load(root_path + "_t2.nii.gz").get_fdata())

        return item + 1

    def compute_folder(self, force=False, verbose=False):

        if self.x_data is None or self.y_data is None:
            raise ValueError("Need to initialize first")

        if not force and self.is_compute:
            print("Skip computation... If you want to force computation, you can use force_compute=True as args")
            return

        item = 0
        names_folders = []
        for i, folder in tqdm(enumerate(os.listdir(self.in_path)), total=self.nb_brains):

            if not os.path.isdir(os.path.join(self.in_path, folder)) or i < self.start_index:
                continue
            elif i >= self.nb_brains + self.start_index:
                break

            if verbose:
                names_folders.append(folder)
            root_path = os.path.join(self.in_path, folder, folder)
            if self.size_crop == (240, 240, 155):
                item = self.compute_one_brain_nocrop(root_path, item)
            else:
                item = self.compute_one_brain_crops(root_path, item)

        if verbose:
            print(names_folders)
        np.memmap.flush(self.x_data)
        del self.x_data
        np.memmap.flush(self.y_data)
        del self.y_data
        size_crop = self.size_crop
        self.x_data = np.memmap(self.x_path, dtype='float32', mode='r',
                                shape=(self.size, size_crop[0], size_crop[1], size_crop[2], 4))
        self.y_data = np.memmap(self.y_path, dtype='float32', mode='r',
                                shape=(self.size, size_crop[0], size_crop[1], size_crop[2], 1))
