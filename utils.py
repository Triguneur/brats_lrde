import os

import matplotlib.pyplot as plt
import numpy as np
import nibabel as nib
import torch

from data.barycenter import *
from data.brats_dataloader3d import BraTsDataset3d




def compute_normalization(data):
    nonzeros = data[data > 0]
    mu = np.mean(nonzeros)
    sigma = np.std(nonzeros)
    if sigma == 0:
        result = data
    else:
        result = np.clip((data - mu) / (12.0 * sigma), -1.0, 1.0)
    return result


def crop_center(img, size):
    y,x = img.shape
    cropx, cropy = size
    if y < cropy or x < cropx:
        raise ValueError("Crop size must be smaller than original size")
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)    
    return img[starty:starty+cropy,startx:startx+cropx]


@torch.no_grad()
def compute_dice(seg,mask):
    epsilon = 1.0
    card_intersection = torch.sum(seg * mask)
    card_seg = torch.sum(seg)
    card_mask = torch.sum(mask)
    numerateur = 2 * card_intersection + epsilon
    denominateur = card_seg + card_mask + epsilon
    return (numerateur/denominateur)


def fast_display(*img2dlst, title=None, display=True, save_path=None, cmap='gray'):
    if title is None:
        title = ""
    plt.figure(figsize=(16,8))
    nbimg = len(img2dlst)
    cols = min(9, nbimg)
    rows = (nbimg // cols) + 1
    for ii, img2d in enumerate(img2dlst):
        plt.subplot(rows, cols, 1+ii)
        plt.imshow(img2d, cmap=cmap)

    if display:
        plt.show()
    if save_path:
        plt.savefig(save_path)


@torch.no_grad()
def evaluate_one_brain(model, path_mri, root_name, size_crop, device):

    model.eval()

    model.to(device)

    flair_name = root_name + '_flair.nii.gz'
    flair = BraTsDataset3d.compute_normalization(nib.load(os.path.join(path_mri, flair_name)).get_fdata())
    flair = torch.from_numpy(add_value(flair, new_shape=(256,256,192), fill_with=flair[0,0,0])).to(device)

    t1_name = root_name + '_t1.nii.gz'
    t1 = BraTsDataset3d.compute_normalization(nib.load(os.path.join(path_mri, t1_name)).get_fdata())
    t1 = torch.from_numpy(add_value(t1, new_shape=(256,256,192), fill_with=t1[0,0,0])).to(device)

    t1ce_name = root_name + '_t1ce.nii.gz'
    t1ce = BraTsDataset3d.compute_normalization(nib.load(os.path.join(path_mri, t1ce_name)).get_fdata())
    t1ce = torch.from_numpy(add_value(t1ce, new_shape=(256,256,192), fill_with=t1ce[0,0,0])).to(device)

    t2_name = root_name + '_t2.nii.gz'
    t2 = BraTsDataset3d.compute_normalization(nib.load(os.path.join(path_mri, t2_name)).get_fdata())
    t2 = torch.from_numpy(add_value(t2, new_shape=(256,256,192), fill_with=t2[0,0,0])).to(device)

    seg_name = root_name + '_seg.nii.gz'
    seg = BraTsDataset3d.compute_normalization(np.clip(nib.load(os.path.join(path_mri, seg_name)).get_fdata(), 0, 1))
    seg = torch.from_numpy(add_value(seg, new_shape=(256,256,192), fill_with=seg[0,0,0])).to(device)


    result = torch.zeros((256, 256, 192)).to(device)
    
    for i in range (0, 240, size_crop[0]):
        for j in range(0, 240, size_crop[1]):
            for k in range (0, 155, size_crop[2]):
                modalites = torch.zeros((1, 4, size_crop[0], size_crop[1], size_crop[2])).to(device)
                modalites[0,0,:,:,:] = flair[i:i+size_crop[0], j:j+size_crop[1], k:k+size_crop[2]]
                modalites[0,1,:,:,:] = t1[i:i+size_crop[0], j:j+size_crop[1], k:k+size_crop[2]]
                modalites[0,2,:,:,:] = t1ce[i:i+size_crop[0], j:j+size_crop[1], k:k+size_crop[2]]
                modalites[0,3,:,:,:] = t2[i:i+size_crop[0], j:j+size_crop[1], k:k+size_crop[2]]
                result[i:i+size_crop[0], j:j+size_crop[1], k:k+size_crop[2]] = \
                    model(modalites)[0, 0, :, :, :]

    result = torch.round(result)
    return result, compute_dice(result, seg)


@torch.no_grad()
def evaluate_on_dataset(model, dataset, display=False, save_path=None):

    if save_path and not os.path.isdir(save_path):
        os.makedirs(save_path)
    liste_dices = []
    current_save = None
    for i, e in enumerate(dataset):
        current_data = torch.unsqueeze(e['data'], 0)
        result = model(current_data)[0, 0, :, :]
        bin_result = torch.round(result)
        current_dice = compute_dice(bin_result, e['label'])
        liste_dices.append(current_dice)
        if save_path:
            current_save = os.path.join(save_path, "%05d.png" % i)
        if display:
            print(f"dice: {current_dice}")
            fast_display(e['data'][0, :, :], result, bin_result, e['label'][0,:,:], display=display, save_path=current_save)
    return liste_dices
