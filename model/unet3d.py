import sys

import torch
from torch import nn
import torchvision
from tqdm import tqdm


class Block(nn.Module):

    def __init__(self, in_ch, out_ch):
        super(Block, self).__init__()
        self.conv1 = nn.Conv3d(in_channels=in_ch, out_channels=out_ch, kernel_size=3, padding=1)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv3d(in_channels=out_ch, out_channels=out_ch, kernel_size=3, padding=1)

    def forward(self, x):
        return self.conv2(self.relu(self.conv1(x)))


class Encoder(nn.Module):

    def __init__(self, nbf=None):
        super(Encoder, self).__init__()

        if nbf is None:
            nbf = [4, 64, 128, 256, 512, 1024]

        self.enc_blocks = nn.ModuleList([Block(nbf[i], nbf[i + 1]) for i in range(len(nbf) - 1)])
        self.pool = nn.MaxPool3d(kernel_size=2)

    def forward(self, x):
        result = list()
        for i, block in enumerate(self.enc_blocks):
            x = block(x)
            result.append(x)
            x = self.pool(x)
        return result


class Decoder(nn.Module):

    def __init__(self, nbf=None):
        super(Decoder, self).__init__()

        if nbf is None:
            nbf = [1024, 512, 256, 128, 64]

        self.nbf = nbf
        self.dec_blocks = nn.ModuleList([Block(nbf[i], nbf[i + 1]) for i in range(len(nbf) - 1)])
        self.up_conv = nn.ModuleList([nn.ConvTranspose3d(nbf[i], nbf[i + 1], 2, 2) for i in range(len(nbf) - 1)])

    def forward(self, x, enc_features):
        for i in range(len(self.nbf) - 1):
            x = self.up_conv[i](x)
            x = torch.cat([x, enc_features[i]], dim=1)
            x = self.dec_blocks[i](x)
        return x

    def crop(self, enc_filters, x):
        _, _, _, height, width = x.shape
        enc_filters = torchvision.transforms.CenterCrop([height, width])(enc_filters)
        return enc_filters


class UNet3d(nn.Module):

    def __init__(self, criterion=torch.nn.BCELoss(), nbf_enc=None, nbf_dec=None, num_class=1):
        super(UNet3d, self).__init__()

        if nbf_dec is None:
            nbf_dec = [1024, 512, 256, 128, 64]

        if nbf_enc is None:
            nbf_enc = [4, 64, 128, 256, 512, 1024]

        self.criterion = criterion
        self.encoder = Encoder(nbf=nbf_enc)
        self.decoder = Decoder(nbf=nbf_dec)
        self.head = nn.Conv3d(nbf_dec[-1], num_class, 1)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        enc_out = self.encoder(x)
        out = self.decoder(enc_out[::-1][0], enc_out[::-1][1:])
        out = self.head(out)
        out = self.sigmoid(out)
        return out

    def get_loss(self, inputs, labels, criterion, opt=None):

        if opt:
            opt.zero_grad()
        outputs = self(inputs)
        loss = criterion(outputs, labels)
        if opt:
            loss.backward()
            opt.step()
        return loss

    def train_model(self, train_loader, optimizer, device, epochs=1, save_path=None, validation_loader=None):
        self.to(device)
        tab_val_loss = [sys.maxsize]
        tab_loss = []

        for epoch in range(epochs):

            self.train()
            running_loss = 0.0
            n_batches = 0
            for i, data in tqdm(enumerate(train_loader), total=len(train_loader)):
                n_batches += 1
                inputs = data['data'].to(device)
                labels = data['label'].to(device)

                loss = self.get_loss(inputs, labels, self.criterion, optimizer)

                running_loss += loss.item()

            self.eval()
            val_loss = 0.0
            n_batches_val = 0
            if validation_loader:
                with torch.no_grad():
                    for data in validation_loader:
                        inputs = data['data'].to(device)
                        labels = data['label'].to(device)
                        val_loss += self.get_loss(inputs, labels, self.criterion)
                        n_batches_val += 1

            if n_batches_val != 0:
                val_loss /= n_batches_val
            if save_path:
                if val_loss <= tab_val_loss[-1]:
                    self.__save_model(save_path, epoch, optimizer, tab_loss, tab_val_loss)
                else:
                    print("val_loss didn't improve restoring weights...")
                    self.load_state_dict(torch.load(save_path)['model_state_dict'])
                    val_loss = tab_val_loss[-1]

            tab_val_loss.append(val_loss)
            tab_loss.append(running_loss / n_batches)
            print(f"Epochs : {epoch + 1}\tloss : {running_loss / n_batches}\tval_loss : {val_loss}")

        return tab_loss, tab_val_loss


    def __save_model(self, save_path, epoch, optimizer, loss, val_loss):
        torch.save({
            'epoch': epoch,
            'model_state_dict': self.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': loss,
            'val_loss': val_loss
        }, save_path)


    @staticmethod
    def load_model(save_path, device='cpu', optimizer=None, nbf_enc=None):
        model = UNet3d(nbf_enc=nbf_enc)
        checkpoint = torch.load(save_path, map_location=device)
        model.load_state_dict(checkpoint['model_state_dict'])
        if optimizer:
            optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        
        return model


class ToTensor(object):

    def __call__(self, sample):
        data, labels = sample['data'], sample['label']

        data = data.transpose((3, 0, 1, 2))
        labels = labels.transpose((3, 0, 1, 2))
        return {'data': torch.from_numpy(data), 'label': torch.from_numpy(labels)}

